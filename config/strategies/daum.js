/**
 * Created by Jack on 1/12/16.
 */
var passport = require('passport'),
    url = require('url'),
    DaumStrategy = require('passport-daum-oauth2').Strategy,
    config = require('../config'),
    usersCtrl = require('../../app/controllers/users_server_controller');

module.exports = function () {
    passport.use(new DaumStrategy({
        clientID: config.daum.clientID,
        clientSecret: config.daum.clientSecret,
        callbackURL: config.daum.callbackURL,
        passReqToCallback: true
    }, function (req, accessToken, refreshToken, profile, done) {
        var providerData = profile._json;
        providerData.accessToken = accessToken;
        providerData.refreshToken = refreshToken;

        var providerUserProfile = {
            userName: profile.displayName,
            provider: 'daum',
            providerId: profile.id,
            providerData: providerData
        };

        usersCtrl.saveOAuthUserProfile(req, providerUserProfile, done);
    }));
};