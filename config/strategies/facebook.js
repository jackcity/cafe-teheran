/**
 * Created by Jack on 1/11/16.
 */
var passport = require('passport'),
    url = require('url'),
    FacebookStrategy = require('passport-facebook').Strategy,
    config = require('../config'),
    usersCtrl = require('../../app/controllers/users_server_controller');

module.exports = function () {
    passport.use(new FacebookStrategy({
        clientID: config.facebook.clientID,
        clientSecret: config.facebook.clientSecret,
        callbackURL: config.facebook.callbackURL,
        profileFields: config.facebook.profileFields,
        passReqToCallback: true
    }, function (req, accessToken, refreshToken, profile, done) {
        var providerData = profile._json;
        providerData.accessToken = accessToken;
        providerData.refreshToken = refreshToken;

        var providerUserProfile = {
            userName: profile.displayName,
            gender: profile.gender,
            birthday: profile.birthday,
            email: profile.emails[0].value,
            provider: 'facebook',
            providerId: profile.id,
            providerData: providerData
        };

        usersCtrl.saveOAuthUserProfile(req, providerUserProfile, done);
    }));
};