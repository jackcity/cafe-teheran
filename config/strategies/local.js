/**
 * Created by Jack on 1/8/16.
 */
var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    User = require('mongoose').model('User');

module.exports = function () {
    passport.use(new LocalStrategy(function (userID, password, done) {
        User.findOne({ userID: userID },
            function (err, user) {
                if (err) {
                    return done(err);
                }

                if (!user) {
                    return done(null, false, { message: '존재하지 않는 아이디입니다.' });
                }

                if (!user.authenticate(password)) {
                    return done(null, false, { message: '비밀번호가 틀렸습니다.' });
                }

                return done(null, user);
            }
        );
    }));
};