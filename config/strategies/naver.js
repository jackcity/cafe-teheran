/**
 * Created by Jack on 1/12/16.
 */
var passport = require('passport'),
    url = require('url'),
    NaverStrategy = require('passport-naver').Strategy,
    config = require('../config'),
    usersCtrl = require('../../app/controllers/users_server_controller');

module.exports = function () {
    passport.use(new NaverStrategy({
        clientID: config.naver.clientID,
        clientSecret: config.naver.clientSecret,
        callbackURL: config.naver.callbackURL,
        passReqToCallback: true
    }, function (req, accessToken, refreshToken, profile, done) {
        var providerData = profile._json;
        providerData.accessToken = accessToken;
        providerData.refreshToken = refreshToken;

        var providerUserProfile = {
            userName: profile.displayName,
            email: profile.emails[0].value,
            provider: 'naver',
            providerId: profile.id,
            providerData: providerData
        };

        usersCtrl.saveOAuthUserProfile(req, providerUserProfile, done);
    }));
};