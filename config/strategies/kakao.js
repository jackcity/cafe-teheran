/**
 * Created by Jack on 1/11/16.
 */
var passport = require('passport'),
    url = require('url'),
    KakaoStrategy = require('passport-kakao').Strategy,
    config = require('../config'),
    usersCtrl = require('../../app/controllers/users_server_controller');

module.exports = function () {
    passport.use(new KakaoStrategy({
        clientID: config.kakao.clientID,
        callbackURL: config.kakao.callbackURL,
        passReqToCallback: true
    }, function (req, accessToken, refreshToken, profile, done) {
        var providerData = profile._json;
        providerData.accessToken = accessToken;
        providerData.refreshToken = refreshToken;

        var providerUserProfile = {
            userName: profile.username,
            provider: 'kakao',
            providerId: profile.id,
            providerData: providerData
        };

        usersCtrl.saveOAuthUserProfile(req, providerUserProfile, done);
    }));
};