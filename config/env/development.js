/**
 * Created by Jack on 1/2/16.
 */
var fs = require('fs');

module.exports = {
    httpsOptions: {
        key: fs.readFileSync('./config/keys/teheran_proto-key.pem'),
        cert: fs.readFileSync('./config/keys/teheran_proto-cert.pem')
    },
    sessionSecret: 'CafeTeheranPrototypeSessionSecret',
    dbUri: 'mongodb://ec2-52-192-155-23.ap-northeast-1.compute.amazonaws.com:27017/teheran-proto',
    facebook: {
        clientID: 'clientID',
        clientSecret: 'clientSecret',
        callbackURL: 'https://ec2-52-192-155-23.ap-northeast-1.compute.amazonaws.com:443/oauth/facebook/callback',
        profileFields: ['id', 'displayName', 'gender', 'birthday', 'email', 'first_name', 'last_name']
    },
    naver: {
        clientID: 'clientID',
        clientSecret: 'clientSecret',
        callbackURL: 'https://ec2-52-192-155-23.ap-northeast-1.compute.amazonaws.com:443/oauth/naver/callback'
    },
    kakao: {
        clientID: 'clientID',
        callbackURL: 'https://ec2-52-192-155-23.ap-northeast-1.compute.amazonaws.com/oauth/kakao/callback'
    },
    daum: {
        clientID: 'clientID',
        clientSecret: 'clientSecret',
        callbackURL: 'https://ec2-52-192-155-23.ap-northeast-1.compute.amazonaws.com:443/oauth/daum/callback'
    }
};