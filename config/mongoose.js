/**
 * Created by Jack on 1/7/16.
 */
var config = require('./config'),
    mongoose = require('mongoose');

module.exports = function () {
    var db = mongoose.connect(config.dbUri);

    require('../app/models/user_server_model');
    require('../app/models/product_server_model');
    require('../app/models/order_server_model');

    return db;
};