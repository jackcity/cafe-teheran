/**
 * Created by Jack on 1/2/16.
 */
var config = require('./config'),
    https = require('https'),
    express = require('express'),
    morgan = require('morgan'),
    compress = require('compression'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    session = require('express-session'),
    flash = require('connect-flash'),
    passport = require('passport');

module.exports = function () {
    var app = express();
    var server = https.createServer(config.httpsOptions, app);

    if (process.env.NODE_ENV === 'development') {
        app.use(morgan('dev'));
    } else if (process.env.NODE_ENV === 'production') {
        app.use(compress());
    }

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(methodOverride());
    app.use(session({
        secret: config.sessionSecret,
        resave: false,
        saveUninitialized: true
    }));

    app.set('views', './app/views');
    app.set('view engine', 'ejs');

    app.use(flash());
    app.use(passport.initialize());
    app.use(passport.session());

    require('../app/routes/index_server_routes.js')(app);
    require('../app/routes/users_server_routes.js')(app);
    require('../app/routes/products_server_routes.js')(app);
    require('../app/routes/orders_server_routes.js')(app);

    app.use(express.static('./public'));

    return server;
};