/**
 * Created by Jack on 1/2/16.
 */
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var mongoose = require('./config/mongoose'),
    express = require('./config/express'),
    passport = require('./config/passport');

var db = mongoose();
var app = express();
var passport = passport();
var port = 443;

app.listen(port);

module.exports = app;

console.log('Server running at https://ec2-52-192-155-23.ap-northeast-1.compute.amazonaws.com:' + port);