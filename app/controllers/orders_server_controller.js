/**
 * Created by Jack on 1/26/16.
 */
var mongoose = require('mongoose'),
    request = require('request'),
    User = mongoose.model('User'),
    Order = mongoose.model('Order');
var errorCtrl = require('./mongooseErrorMessage_server_controller');

exports.create = function (req, res) {
    var order = new Order(req.body);
    order.customer = req.user;

    var shopUrl = 'http://ec2-52-192-155-23.ap-northeast-1.compute.amazonaws.com:4761/staffs/orders';

    //Update user information.
    if (!req.user.phone || req.user.address) {
        User.findByIdAndUpdate(req.user.id, {
            phone: req.body.phone,
            address: req.body.address
        }, function (err) {
            if (err) {
                return res.status(400).send({ message: errorCtrl.getErrorMessage(err) });
            }
        });
    }

    order.save(function (err) {
        if (err) {
            return res.status(400).send({ message: errorCtrl.getErrorMessage(err) });
        } else {
            res.json(order);
        }
    });

    request({
        url: shopUrl,
        method: 'POST',
        json: req.body
    },
    function (error, response, body) {
        if (!error && response.statusCode === 200) {
            console.log(body);
        } else {
            console.log(error);
        }
    });
};

exports.list = function (req, res) {
    Order.find().exec(function (err, orders) {
        if (err) {
            return res.status(400).send({ message: errorCtrl.getErrorMessage(err) });
        } else {
            res.json(orders);
        }
    });
};

exports.orderByID = function (req, res, next, id) {
    Order.findById(id).populate('customer', 'userName phone address')
        .exec(function (err, order) {
            if (err) return next(err);
            if (!order) return next(new Error('Failed to load order ' + id));

            req.order = order;
            next();
        });
};

exports.read = function (req, res) {
    res.json(req.order);
};