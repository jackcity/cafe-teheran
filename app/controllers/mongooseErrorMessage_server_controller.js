/**
 * Created by Jack on 1/26/16.
 */
exports.getErrorMessage = function (err) {
    if (err.errors) {
        for (var errName in err.errors) {
            if (err.errors[errName].message) {
                return err.errors[errName].message;
            }
        }
    } else {
        return '알 수 없는 서버에러가 발생했습니다';
    }
};