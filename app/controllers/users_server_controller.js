/**
 * Created by Jack on 1/7/16.
 */
//Local Strategy.
var User = require('mongoose').model('User'),
    passport = require('passport');
var errorCtrl = require('./mongooseErrorMessage_server_controller');

var getErrorMessage = function (err) {
    var message = '';

    if (err.code) {
        switch (err.code) {
            case 11000:
            case 11001:
                message = '이미 있는 아이디입니다.';
                break;
            default:
                message = '무언가 잘못되었군요.';
        }
    } else {
        for (var errName in err.errors) {
            if (err.errors[errName].message) {
                message = err.errors[errName].message;
            }
        }
    }

    return message;
};

exports.renderSignin = function (req, res, next) {
    if (!req.user) {
        res.render('signin', {
            title: 'Cafe Teheran Login',
            messages: req.flash('error') || req.flash('info')
        });
    } else {
        return res.redirect('/');
    }
};

exports.renderSignup = function (req, res, next) {
    if (!req.user) {
        res.render('signup', {
            title: '회원가입',
            messages: req.flash('error')
        });
    } else {
        return res.redirect('/');
    }
};

exports.signup = function (req, res, next) {
    if (!req.user) {
        var user = new User(req.body);
        var message = null;

        user.provider = 'local';

        user.save(function (err) {
            if (err) {
                var message = getErrorMessage(err);

                req.flash('error', message);

                return res.redirect('/signup');
            }

            req.login(user, function (err) {
                if (err) return next(err);

                return res.redirect('/');
            });
        });
    } else {
        return res.redirect('/');
    }
};

exports.signout = function (req, res) {
    req.logout();
    res.redirect('/');
};

//To save OAuth profile.
exports.saveOAuthUserProfile = function (req, profile, done) {
    User.findOne({
        provider: profile.provider,
        providerId: profile.providerId
    }, function (err, user) {
        if (err) {
            return done(err);
        } else {
            if (!user) {
                var possibleUserID = profile.providerId;
                //I used "profile.providerId" because it is only unique one in a profile among the strategies.

                User.findUniqueUserID(possibleUserID, null, function (availableUserID) {
                    profile.userID = availableUserID;

                    user = new User(profile);

                    //If there is 'required' property at "phone" and "address" in 'UserSchema',
                    //error occurs and the code below does not work. There may be some problems.
                    user.save(function (err) {
                        if (err) {
                            var message = _this.getErrorMessage(err);

                            req.flash('error', message);
                            return res.redirect('/signup');
                        }

                        return done(err, user);
                    });
                });
            } else {
                return done(err, user);
            }
        }
    });
};