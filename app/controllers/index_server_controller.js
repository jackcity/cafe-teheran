/**
 * Created by Jack on 1/2/16.
 */
exports.render = function (req, res) {
    if (req.session.lastVisit) {
        console.log(req.session.lastVisit);
    }

    req.session.lastVisit = new Date();

    res.render('index', {
        title: 'Cafe Teheran',
        user: JSON.stringify(req.user)
    });
};