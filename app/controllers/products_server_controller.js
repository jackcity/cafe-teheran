/**
 * Created by Jack on 1/20/16.
 */
var Product = require('mongoose').model('Product');
var errorCtrl = require('./mongooseErrorMessage_server_controller');

exports.list = function (req, res) {
    Product.find().sort('name').exec(function (err, products) {
        if (err) {
            return res.status(400).send({ message: errorCtrl.getErrorMessage(err) });
        } else {
            res.json(products);
        }
    });
};

exports.productByID = function (rea, res, next, id) {
    Product.findById(id).exec(function (err, product) {
        if (err) return next(err);
        if (!product) return next(new Error(id + '제품을 불러오는데 실패했습니다'));

        req.product = product;
        next();
    });
};

exports.readOne = function (req, res) {
    res.json(req.product);
};