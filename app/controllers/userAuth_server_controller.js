/**
 * Created by Jack on 1/14/16.
 */
//To check if a user logged in. (for Authentication)
exports.requiresLogin = function (req, res, next) {
    if (!req.isAuthenticated()) {
        return res.status(401).send({ message: '로그인을 하지 않은 사용자입니다.' });
    }

    next();
};

//To check if a user has an authorization to do the work.
exports.hasAuthorization = function (req, res, next) {
    if (req.someThing.someOne.id !== req.user.id) {
        return res.status(403).send({ message: '권한이 없는 사용자입니다.' });
    }

    next();
};