/**
 * Created by Jack on 1/26/16.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var OrderSchema = new Schema({
    //name: {
    //    type: String,
    //    required: true
    //},
    //phone: {
    //    type: Array,
    //    required: true
    //},
    //address: {
    //    type: String,
    //    required: true
    //},
    customer: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    option: Array,
    product: Array,
    payment: {},
    created: {
        type: Date,
        default: Date.now
    }
});

mongoose.model('Order', OrderSchema);