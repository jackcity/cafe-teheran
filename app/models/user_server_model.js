/**
 * Created by Jack on 1/7/16.
 */
var mongoose = require('mongoose'),
    crypto = require('crypto'),
    Schema = mongoose.Schema;

var UserSchema = new Schema({
    userName: {
        type: String,
        required: true
    },
    phone: Array,
    address: Array,
    email: {
        type: String,
        match: [
            /.+\@.+\..+/,
            '올바른 이메일 주소를 입력해 주세요'
        ]
    },
    gender: String,
    birthday: String,
    age: String,
    userID: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        validate: [
            function(password) {
                return password.length >= 8;
            },
            '비밀번호는 최소 여덟자 이상이어야 합니다'
        ]
    },
    salt: String,
    provider: String,
    providerId: String,
    providerData: {},
    created: {
        type: Date,
        default: Date.now,
        required: true
    },
    orderData: [{
        type: Schema.ObjectId,
        ref: 'Order'
    }]
});

//UserSchema.virtual('fullName').get(function () {
//    if (!this.firstName) {
//        return this.username;
//    } else {
//        return this.lastName + this.firstName;
//    }
//});

UserSchema.pre('save', function (next) {
    if (this.password) {
        this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
        this.password = this.hashPassword(this.password);
    }

    next();
});

UserSchema.methods.hashPassword = function (password) {
    return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
};

UserSchema.methods.authenticate = function (password) {
    return this.password === this.hashPassword(password);
};

UserSchema.statics.findUniqueUserID = function (userID, suffix, callback) {
    var _this = this;
    var possibleUserID = userID + (suffix || '');

    _this.findOne({ userID: possibleUserID },
        function (err, user) {
            if (!err) {
                if (!user) {
                    callback(possibleUserID);
                } else {
                    return _this.findUniqueUserID(userID,
                        (suffix || 0) +1,
                        callback
                    );
                }
            } else {
                callback(null);
            }
        }
    );
};

UserSchema.set('toJSON', {
    getters: true,
    virtuals: true
});

mongoose.model('User', UserSchema);