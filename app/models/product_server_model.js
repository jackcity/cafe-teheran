/**
 * Created by Jack on 1/20/16.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ProductSchema = new Schema({
    category: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    prices: [{
        size: {
            type: String,
            required: true
        },
        price: {
            type: Number,
            required: true
        }
    }]
});

mongoose.model('Product', ProductSchema);