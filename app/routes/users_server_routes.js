/**
 * Created by Jack on 1/7/16.
 */
var usersCtrl = require('../controllers/users_server_controller'),
    passport = require('passport');

module.exports = function (app) {
    //Local Strategy
    app.route('/signup')
        .get(usersCtrl.renderSignup)
        .post(usersCtrl.signup);

    app.route('/signin')
        .get(usersCtrl.renderSignin)
        .post(passport.authenticate('local', {
            successRedirect: '/',
            failureRedirect: '/signin',
            failureFlash: true
        }));

    app.get('/signout', usersCtrl.signout);

    //Facebook Strategy
    app.get('/oauth/facebook', passport.authenticate('facebook', {
        failureRedirect: '/signin'
    }));

    app.get('/oauth/facebook/callback', passport.authenticate('facebook', {
        failureRedirect: '/signin',
        successRedirect: '/'
    }));

    //Naver Strategy
    app.get('/oauth/naver', passport.authenticate('naver', {
        failureRedirect: '/signin'
    }));

    app.get('/oauth/naver/callback', passport.authenticate('naver', {
        failureRedirect: '/signin',
        successRedirect: '/'
    }));

    //Kakao Strategy
    app.get('/oauth/kakao', passport.authenticate('kakao', {
        failureRedirect: '/signin'
    }));

    app.get('/oauth/kakao/callback', passport.authenticate('kakao', {
        failureRedirect: 'signin',
        successRedirect: '/'
    }));

    //Daum Strategy
    app.get('/oauth/daum', passport.authenticate('daum', {
        failureRedirect: 'signin'
    }));

    app.get('/oauth/daum/callback', passport.authenticate('daum', {
        failureRedirect: 'signin',
        successRedirect: '/'
    }));
};