/**
 * Created by Jack on 1/26/16.
 */
var ordersCtrl = require('../controllers/orders_server_controller'),
    userAuthCtrl = require('../controllers/userAuth_server_controller');

module.exports = function (app) {
    app.route('/api/orders')
        .get(ordersCtrl.list)
        .post(userAuthCtrl.requiresLogin, ordersCtrl.create);

    app.route('/api/orders/:orderId').get(ordersCtrl.read);

    app.param('orderId', ordersCtrl.orderByID);
};