/**
 * Created by Jack on 1/2/16.
 */
module.exports = function (app) {
    var indexCtrl = require('../controllers/index_server_controller');

    app.get('/', indexCtrl.render);
};