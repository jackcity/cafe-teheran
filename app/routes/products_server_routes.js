/**
 * Created by Jack on 1/20/16.
 */
var productsCtrl = require('../controllers/products_server_controller');

module.exports = function (app) {
    app.route('/api/products').get(productsCtrl.list);

    app.route('/api/products/:productId').get(productsCtrl.readOne);

    app.param('productId', productsCtrl.productByID);
};