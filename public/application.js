/**
 * Created by Jack on 1/13/16.
 */
var mainApplicationModuleName = 'cafeTeheranProto';

var mainApplicationModule = angular.module(mainApplicationModuleName,
    ['ngResource', 'ngRoute', 'user', 'main', 'cart', 'order', 'payment']);

mainApplicationModule.config(['$locationProvider', function ($locationProvider) {
    $locationProvider.hashPrefix('!');
}]);

if (window.location.hash === '#_=_') window.location.hash = '#!';

angular.element(document).ready(function () {
    angular.bootstrap(document, [mainApplicationModuleName]);
});