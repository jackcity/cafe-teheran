/**
 * Created by Jack on 1/21/16.
 */
angular.module('cart').factory('Cart', ['Data', function (Data) {
    var cartData = Data.data.cart;

    return {
        addProduct: function (productId, name, sizeId, size, price) {
            var addedToExistingItem = false;

            for (var i = 0; i < cartData.length; i++) {
                if (cartData[i].sizeId === sizeId) {
                    cartData[i].count++;
                    addedToExistingItem = true;
                    break;
                }
            }

            if (!addedToExistingItem) {
                cartData.push({
                    productId: productId,
                    name: name,
                    sizeId: sizeId,
                    size: size,
                    price: price,
                    count: 1
                });
            }
        },
        removeProduct: function (sizeId) {
            for (var i = 0; i < cartData.length; i++) {
                if (cartData[i].sizeId === sizeId) {
                    cartData.splice(i, 1);
                    break;
                }
            }
        },
        listProducts: function () {
            return cartData;
        }
    };
}]);