/**
 * Created by Jack on 2/27/16.
 */
angular.module('cart').factory('Data', [function () {
    return {
        data: {
            cart: [],
            shipping: {
                name: '',
                phone: [],
                address: ['서울시 강남구'],
                option: []
            },
            payment: {
                coupon: 'none',
                total: 0,
                paymentMethod: '현장 카드결제',
                paid: false
            }
        }
    };
}]);