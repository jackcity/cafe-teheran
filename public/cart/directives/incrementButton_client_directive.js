/**
 * Created by Jack on 1/22/16.
 */
angular.module('cart').directive('incButton', [function () {
    return {
        restrict: 'E',
        scope: {
            value: '=value'
        },
        link: function (scope, element) {
            var button = angular.element('<button>').text('+');

            element.append(button);
            button.on('click', function () {
                scope.$apply(function () {
                    scope.value++;
                });
            });
        }
    };
}]);