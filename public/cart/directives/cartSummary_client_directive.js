/**
 * Created by Jack on 1/21/16.
 */
angular.module('cart').directive('cartSummary', ['Cart', function (Cart) {
    return {
        restrict: 'E',
        templateUrl: '/cart/views/cartSummary_client_view.html',
        controller: function ($scope) {
            var cartData = Cart.listProducts();

            $scope.total = function () {
                var total = 0;

                for (var i = 0; i < cartData.length; i++) {
                    total += (cartData[i].price * cartData[i].count);
                }

                return total;
            };

            $scope.itemCount = function () {
                var total = 0;

                for (var i = 0; i < cartData.length; i++) {
                    total += cartData[i].count;
                }

                return total;
            };
        }
    };
}]);