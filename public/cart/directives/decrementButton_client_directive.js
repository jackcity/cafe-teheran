/**
 * Created by Jack on 1/22/16.
 */
angular.module('cart').directive('decButton', ['Cart', function (Cart) {
    return {
        restrict: 'E',
        scope: {
            value: '=value',
            sizeId: '=id'
        },
        link: function (scope, element) {
            var button = angular.element('<button>').text('-');

            element.append(button);
            button.on('click', function () {
                scope.$apply(function () {
                    if (scope.value > 1) {
                        scope.value--;
                    } else if (scope.value === 1) {
                        Cart.removeProduct(scope.sizeId);
                    }
                });
            });
        }
    };
}]);