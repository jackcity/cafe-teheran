/**
 * Created by Jack on 1/22/16.
 */
angular.module('cart').controller('CartCtrl', ['$scope', 'Cart', 'Data',
    function ($scope, Cart, Data) {
        $scope.cartData = Cart.listProducts();

        $scope.total = function () {
            var total = 0;

            for (var i = 0; i < $scope.cartData.length; i++) {
                total += ($scope.cartData[i].price * $scope.cartData[i].count);
            }

            return total;
        };

        Data.data.payment.total = $scope.total();

        $scope.remove = function (sizeId) {
            Cart.removeProduct(sizeId);
        };
    }
]);