/**
 * Created by Jack on 1/26/16.
 */
angular.module('order').controller('OrderCtrl',
    ['$scope', '$location', 'Data', 'Authentication',
        function ($scope, $location, Data, Authentication) {
            $scope.data = Data.data;

            $scope.data.shipping.name = Authentication.user.userName;

            if (Authentication.user.phone) {
                $scope.data.shipping.phone = Authentication.user.phone;
            }

            if (Authentication.user.address) {
                $scope.data.shipping.address[1] = Authentication.user.address[1];
            }

            $scope.shippingOptionList = [
                '도착하면 전화주세요',
                '제품을 바로 받을 수 있게 테이크아웃 용기에 담아주세요',
                '1층 로비에서 수령할께요'
            ];

            $scope.pushOptions = function (selectedOpt) {
                var shippingOption = $scope.data.shipping.option;

                if (shippingOption.length === 0) {
                    if (selectedOpt !== null && selectedOpt !== '') {
                        shippingOption.push(selectedOpt);
                        //$scope.data.shipping.option = angular.copy(shippingOption);
                    }
                } else {
                    if (shippingOption.indexOf(selectedOpt) === -1) {
                        if (selectedOpt !== null && selectedOpt !== '') {
                            shippingOption.push(selectedOpt);
                            //$scope.data.shipping.option = angular.copy(shippingOption);
                        }
                    }
                }
            };

            $scope.pushCustomOptions = function (customOpt) {
                var shippingOption = $scope.data.shipping.option;

                if (shippingOption.length === 0) {
                    if (customOpt !== null && customOpt !== '') {
                        shippingOption.push(customOpt);
                        //$scope.data.shipping.option = angular.copy(shippingOption);
                    }
                } else {
                    if (shippingOption.indexOf(customOpt) === -1 && shippingOption.length < 4) {
                        if (customOpt !== null && customOpt !== '') {
                            shippingOption.push(customOpt);
                            //$scope.data.shipping.option = angular.copy(shippingOption);
                        }
                    }
                }
            };

            $scope.removeOption = function ($index) {
                var shippingOption = $scope.data.shipping.option;

                for (var i = 0; i < shippingOption.length; i++) {
                    if (shippingOption[i] === shippingOption[$index]) {
                        shippingOption.splice(i, 1);
                        break;
                    }
                }
            };
        }
    ]
);
