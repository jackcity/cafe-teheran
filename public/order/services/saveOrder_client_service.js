/**
 * Created by Jack on 1/26/16.
 */
angular.module('order').factory('SaveOrder', ['$resource', function ($resource) {
    return $resource('api/orders/:orderId', { orderId: '@_id' });
}]);