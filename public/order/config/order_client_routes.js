/**
 * Created by Jack on 1/27/16.
 */
angular.module('order').config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/placeorder', { templateUrl: '/order/views/placeOrder_client_view.html' })
        .when('/complete', { templateUrl: '/order/views/thankYou.html' });
}]);