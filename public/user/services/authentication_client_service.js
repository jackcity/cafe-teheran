/**
 * Created by Jack on 1/13/16.
 */
angular.module('user').factory('Authentication', [function () {
    this.user = window.user;

    return { user: this.user };
}]);