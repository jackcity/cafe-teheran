/**
 * Created by Jack on 1/13/16.
 */
angular.module('main').config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/', { templateUrl: '/main/views/mainMenuList_client_view.html' })
        .when('/cart', { templateUrl: '/cart/views/cart_client_view.html' })
        .otherwise({ redirectTo: '/' });
}]);