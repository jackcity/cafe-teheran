/**
 * Created by Jack on 1/13/16.
 */
angular.module('main').controller('MainCtrl',
    ['$scope', '$route', '$routeParams', 'Authentication', 'LoadProduct', 'Cart',
        function ($scope, $route, $routeParams, Authentication, LoadProduct, Cart) {
            $scope.name = Authentication.user ? Authentication.user.userName : '고객';

            $scope.authentication = Authentication;
            //You can use this for showing something to a specific someone with like "ng-show".
            //ex) authentication.user, authentication.user._id === something.someone._id

            $scope.productsList = function () {
                $scope.products = LoadProduct.query();
            };

            $scope.readOneProduct = function () {
                $scope.product = LoadProduct.get({ productId: $routeParams.productId });
            };

            $scope.addRsProductToCart = function (product) {
                Cart.addProduct(product._id, product.name, product.prices[0]._id,
                    product.prices[0].size, product.prices[0].price);
            };

            $scope.addLsProductToCart = function (product) {
                Cart.addProduct(product._id, product.name, product.prices[1]._id,
                    product.prices[1].size, product.prices[1].price);
            };
        }
    ]
);