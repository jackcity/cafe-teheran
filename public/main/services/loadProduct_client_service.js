/**
 * Created by Jack on 1/20/16.
 */
angular.module('main').factory('LoadProduct', ['$resource', function ($resource) {
    return $resource('api/products/:productId', { productId: '@_id' });
}]);