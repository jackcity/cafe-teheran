/**
 * Created by Jack on 3/8/16.
 */
angular.module('payment')
    .controller('PaymentCtrl', ['$scope', '$location', 'Authentication', 'Data', 'SaveOrder',
        function ($scope, $location, Authentication, Data, SaveOrder) {
            //For OrderSchema.
            $scope.orderData = {};
            $scope.orderData.product = Data.data.cart;
            $scope.orderData.phone = Data.data.shipping.phone;
            $scope.orderData.address = Data.data.shipping.address;
            $scope.orderData.option = Data.data.shipping.option;
            $scope.orderData.payment = Data.data.payment;

            for (var i in $scope.orderData.product) {
                delete($scope.orderData.product[i].productId);
                delete($scope.orderData.product[i].sizeId);
                delete($scope.orderData.product[i].price);
            }

            $scope.sendOrder = function (orderData) {
                new SaveOrder(orderData).$save(function (response) {
                    $scope.user = Authentication.user;

                    //need to remove all data for order.
                }, function (errorResponse) {
                    $scope.data.orderError = errorResponse.data.message;
                });
            };
        }
    ]
);