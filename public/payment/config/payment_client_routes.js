/**
 * Created by Jack on 3/8/16.
 */
angular.module('payment').config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/payment', { templateUrl: '/payment/views/payment_client_view.html' })
        .when('/complete', { templateUrl: '/payment/views/completeOrder_client_view.html' });
}]);